var PorscheCar = /** @class */ (function () {
    function PorscheCar(params) {
        this.engine = params.engine;
        this.tyreModel = params.tyreModel;
        this.gears = params.gears;
    }
    return PorscheCar;
}());
var PorscheCarFactory = /** @class */ (function () {
    function PorscheCarFactory() {
    }
    PorscheCarFactory.prototype.createCar = function (params) {
        return new PorscheCar(params);
    };
    return PorscheCarFactory;
}());
var FerrariCar = /** @class */ (function () {
    function FerrariCar(params) {
        var bolts = [];
        bolts.push("");
        this.engine = params.engine;
        this.tyreModel = params.tyreModel;
        this.gears = params.gears;
        this.turbo = params.turbo;
    }
    FerrariCar.prototype.loadSoftware = function () {
        var password = "intel@123TheBestPwdEver;)";
        return password;
    };
    return FerrariCar;
}());
var FerrariCarFactory = /** @class */ (function () {
    function FerrariCarFactory() {
    }
    FerrariCarFactory.prototype.createCar = function (params) {
        return new FerrariCar(params);
    };
    return FerrariCarFactory;
}());
var CarFactoryImpl = /** @class */ (function () {
    function CarFactoryImpl() {
        this._porscheCar = new PorscheCarFactory();
        this._ferrariCar = new FerrariCarFactory();
    }
    CarFactoryImpl.prototype.getPorsche = function () {
        debugger;
        return this._porscheCar;
    };
    CarFactoryImpl.prototype.getFerrari = function () {
        return this._ferrariCar;
    };
    CarFactoryImpl.prototype.setSpareWheel = function (whl) {
        this._spareWheel = whl;
    };
    CarFactoryImpl.prototype.setRoof = function (rf) {
        this._spareWheel = rf;
    };
    CarFactoryImpl.prototype.getSpareWheel = function () {
        return this._spareRoof;
    };
    CarFactoryImpl.prototype.getspareRoof = function () {
        return this._spareWheel;
    };
    return CarFactoryImpl;
}());
var fact = new CarFactoryImpl();
var porscheParams = { engine: "raw", tyreModel: "basic", gears: 1 };
var porscheCar = fact.getPorsche().createCar(porscheParams);
console.log(porscheCar.engine);
var ferrariParams = { engine: "rawraw", tyreModel: "basic", gears: 2, turbo: true };
var ferrariCar = fact.getFerrari().createCar(ferrariParams);
console.log(ferrariCar.turbo);
console.log(ferrariCar.loadSoftware());
