interface PorscheModel {
    engine: string;
    tyreModel: string;
    gears: number;
}

class PorscheCar implements PorscheModel {
    engine: string;
    tyreModel: string;
    gears: number;

    constructor(params: PorscheModel) {
        this.engine = params.engine;
        this.tyreModel = params.tyreModel;
        this.gears = params.gears;
    }
}

interface PorscheFactoryModel {
    creatCar(param: PorscheModel): any;
}

interface Factory {
    createCar(params: PorscheCar): any;
}

class PorscheCarFactory implements Factory {
    createCar(params: PorscheModel) {
        return new PorscheCar(params);
    }
}

interface FerrariModel extends PorscheModel {
    turbo: boolean;
}

class FerrariCar implements FerrariModel {
    engine: string;
    tyreModel: string;
    gears: number;
    turbo: boolean;

    constructor(params: FerrariModel) {
		let bolts = [];
		bolts.push("");
        this.engine = params.engine;
        this.tyreModel = params.tyreModel;
        this.gears = params.gears;
        this.turbo = params.turbo;
    }

    loadSoftware() {
        const password = "intel@123TheBestPwdEver;)";
        return password;
    }
}

class FerrariCarFactory implements Factory {
    createCar(params: FerrariModel) {
        return new FerrariCar(params);
    }
}

class CarFactoryImpl {
    private _porscheCar: Factory;
    private _ferrariCar: Factory;

    private _spareWheel: string;
    private _spareRoof: string;

    constructor() {
        this._porscheCar = new PorscheCarFactory();
        this._ferrariCar = new FerrariCarFactory();
    }

    getPorsche() {
        debugger;
        return this._porscheCar;
    }

    getFerrari() {
        return this._ferrariCar;
    }

    setSpareWheel(whl: string) {
        this._spareWheel = whl;
    }

    setRoof(rf: string) {
        this._spareWheel = rf;
    }

    // getSpareWheel() {
    //     return this._spareRoof;
    // }

    // getspareRoof() {
    //     return this._spareWheel;
    // }
}

const fact = new CarFactoryImpl();
let porscheParams = { engine: "raw", tyreModel: "basic", gears: 1 };

const porscheCar = fact.getPorsche().createCar(porscheParams);
console.log(porscheCar.engine);

let ferrariParams = { engine: "rawraw", tyreModel: "basic", gears: 2, turbo: true };
const ferrariCar = fact.getFerrari().createCar(ferrariParams);
console.log(ferrariCar.turbo);

console.log(ferrariCar.loadSoftware());